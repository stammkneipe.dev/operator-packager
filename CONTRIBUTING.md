# Contributing

### Thank you for you interest in Operator Packager!  You are welcome here.

The Operator Packager is an open source project and always open for outside contributors.

If you are looking for a place to start take a look at the [open issues](/issues).

## Testing

Before submitting a merge request, make sure that our Gitlab CI Test Pipeline will succeed.
