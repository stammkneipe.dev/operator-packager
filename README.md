# Operator Packager

> A Docker Image to generate and package Helm Charts for the Operator SDK

## Usage

```yaml
package_and_upload:
  variables:
    HELM_EXPERIMENTAL_OCI: 1
    CHART: mychart
    VERSION: 0.0.1
  image: registry.gitlab.com/stammkneipe-dev/operator-packager:latest
  script:
    - kustomize build config/default | helmify
    - helm package chart
    - helm repo add --username gitlab-ci-token --password ${CI_JOB_TOKEN} ${CI_PROJECT_NAME} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable
    - helm plugin install https://github.com/chartmuseum/helm-push
    - helm cm-push ${CHART}-${VERSION}.tgz ${CI_PROJECT_NAME}
```

## Batteries Included

- [curl](https://github.com/curl/curl)
- [git](https://github.com/git/git)
- [go](https://github.com/golang/go)
- [kubectl](https://github.com/kubernetes/kubectl)
- [kustomize](https://github.com/kubernetes-sigs/kustomize)
- [helm](https://github.com/helm/helm)
- [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder)
- [helmify](https://github.com/arttor/helmify)

## Contributing

[Contributing](/CONTRIBUTING.md)

## License

[MIT License](/LICENSE)

## Authors and acknowledgment

- [Patrick Domnick](https://gitlab.com/PatrickDomnick)
- [Henry Sachs](https://gitlab.com/DerAstronaut)
