# [1.1.0](https://gitlab.com/stammkneipe-dev/operator-packager/compare/1.0.4...1.1.0) (2024-08-10)


### Bug Fixes

* root ([3653dd9](https://gitlab.com/stammkneipe-dev/operator-packager/commit/3653dd911f60bb99e8f6e5aeb2e27159170e1ded))


### Features

* add operator-sdk ([685fbd9](https://gitlab.com/stammkneipe-dev/operator-packager/commit/685fbd99d4c4625cd2611bd2ec54514874ab285f))

## [1.0.4](https://gitlab.com/stammkneipe-dev/operator-packager/compare/1.0.3...1.0.4) (2024-03-17)


### Bug Fixes

* remove tag ([5b3c61b](https://gitlab.com/stammkneipe-dev/operator-packager/commit/5b3c61bc978ad9778c16771974086a3cafb6936a))

## [1.0.3](https://gitlab.com/stammkneipe-dev/operator-packager/compare/1.0.2...1.0.3) (2024-03-16)


### Bug Fixes

* rename ([67e8d0a](https://gitlab.com/stammkneipe-dev/operator-packager/commit/67e8d0a5f5acffa59ffabfe9351d52433477c40a))

## [1.0.1](https://gitlab.com/stammkneipe-dev/operator-packager/compare/1.0.0...1.0.1) (2024-01-27)


### Bug Fixes

* context ([a88d47f](https://gitlab.com/stammkneipe-dev/operator-packager/commit/a88d47ff945b7bdd5e81b5e2ff1957394f5d2035))
* namespace ([b2c0341](https://gitlab.com/stammkneipe-dev/operator-packager/commit/b2c0341996322fc0cb4d2a2208922c527e0b9a7b))

# 1.0.0 (2024-01-27)


### Bug Fixes

* add key ([82f61b4](https://gitlab.com/stammkneipe-dev/operator-packager/commit/82f61b4d668498d0f21ff9cebdf61d50c8b65992))
* add yq ([683bb12](https://gitlab.com/stammkneipe-dev/operator-packager/commit/683bb12292928ca35d1407fed94c31ef5735ecab))
* build ([0b4c4c0](https://gitlab.com/stammkneipe-dev/operator-packager/commit/0b4c4c097cb62c153e97b1afe4f437fbaae9a64d))
* build command ([551b237](https://gitlab.com/stammkneipe-dev/operator-packager/commit/551b23700800843bc5b1a0d3b2864f2302bc2bed))
* certs ([5aec3f7](https://gitlab.com/stammkneipe-dev/operator-packager/commit/5aec3f7421c847bfa2e69a08ad4cb647c2b79d49))
* chartmuseum ([91c9f6d](https://gitlab.com/stammkneipe-dev/operator-packager/commit/91c9f6d50b26c2a6521289cf7ed4ad2834e78482))
* curl and git ([6538303](https://gitlab.com/stammkneipe-dev/operator-packager/commit/6538303ef34725df0b341aa6f43df0cdd34c8ec9))
* helmify ([c9f046e](https://gitlab.com/stammkneipe-dev/operator-packager/commit/c9f046e155dedd4dd7b4965fa4e0b783802f16a7))
* make ([315128f](https://gitlab.com/stammkneipe-dev/operator-packager/commit/315128f16325d11a4f8d982fdcc9f70c8f37cc0a))
* rsa ([aa75672](https://gitlab.com/stammkneipe-dev/operator-packager/commit/aa7567255d605ca732d4ad6449092c8cc2b23e70))


### Features

* add gcc ([347670a](https://gitlab.com/stammkneipe-dev/operator-packager/commit/347670aca002a55b7fc116aab0b35aef3b978b7e))
* add helmify ([19c1cdf](https://gitlab.com/stammkneipe-dev/operator-packager/commit/19c1cdf923ad0c96da51558d41c87927f7960b82))
* add make ([de3d6a5](https://gitlab.com/stammkneipe-dev/operator-packager/commit/de3d6a596e0cf3a181aeb7035a33fbd18c5afdd7))
* Create the best Helm Packager ([dbc147f](https://gitlab.com/stammkneipe-dev/operator-packager/commit/dbc147f4c688af529ad0517f9b1e811c573da059))
* new pipeline ([ea5c5db](https://gitlab.com/stammkneipe-dev/operator-packager/commit/ea5c5db90a325f92b3ff9a090440b66aece92be5))
